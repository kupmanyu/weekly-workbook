#include <CanvasTriangle.h>
#include <DrawingWindow.h>
#include <Utils.h>
#include <fstream>
#include <vector>
#include <glm/glm.hpp>
#include <CanvasPoint.h>
#include <Colour.h>
#include <CanvasTriangle.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <TextureMap.h>
#include <TexturePoint.h>

#define WIDTH 320
#define HEIGHT 240

using namespace std;

vector<float> interpolateSingleFloats(float from, float to, int numberOfValues) {
	float step = (to - from) / (numberOfValues - 1);
	vector<float> result;
	result.push_back(from);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(result[i - 1] + step);
	}
	return result;
}

vector<glm::vec3> interpolateThreeElementValues(glm::vec3 from, glm::vec3 to, int numberOfValues) {
	glm::vec3 step = glm::vec3(
		(to.x - from.x) / (numberOfValues - 1),
		(to.y - from.y) / (numberOfValues - 1),
		(to.z - from.z) / (numberOfValues - 1)
	);
	vector<glm::vec3> result;
	result.push_back(from);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(glm::vec3(result[i - 1].x + step.x, result[i - 1].y + step.y, result[i - 1].z + step.z));
	}
	return result;
}

vector<CanvasPoint> interpolateCanvasPoint(CanvasPoint a, CanvasPoint b, int numberOfValues) {
	glm::vec2 step = glm::vec2(
		(b.x - a.x) / (numberOfValues - 1),
		(b.y - a.y) / (numberOfValues - 1)
	);
	vector<CanvasPoint> result;
	result.push_back(a);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(CanvasPoint(result[i - 1].x + step.x, result[i - 1].y + step.y));
	}
	return result;
}

vector<CanvasPoint> interpolateTexturePoint(CanvasPoint a, CanvasPoint b, int numberOfValues) {
	glm::vec4 step = glm::vec4(
		(b.x - a.x) / (numberOfValues - 1),
		(b.y - a.y) / (numberOfValues - 1),
		(b.texturePoint.x - a.texturePoint.x) / (numberOfValues - 1),
		(b.texturePoint.y - a.texturePoint.y) / (numberOfValues - 1)
	);
	vector<CanvasPoint> result;
	result.push_back(a);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(CanvasPoint(result[i - 1].x + step.x, result[i - 1].y + step.y));
		result[i].texturePoint = TexturePoint(result[i - 1].texturePoint.x + step.z, result[i - 1].texturePoint.y + step.w);
	}
	return result;
}

vector<TexturePoint> interpolateTPoint(TexturePoint a, TexturePoint b, int numberOfValues) {
	glm::vec2 step = glm::vec2(
		(b.x - a.x) / (numberOfValues - 1),
		(b.y - a.y) / (numberOfValues - 1)
	);
	vector<TexturePoint> result;
	result.push_back(a);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(TexturePoint(result[i - 1].x + step.x, result[i - 1].y + step.y));
	}
	return result;
}

uint32_t pack(float alpha, float red, float green, float blue) {
	return (int(alpha) << 24) + (int(red) << 16) + (int(green) << 8) + int(blue);
}

uint32_t pack_vec3(float alpha, glm::vec3 rgb) {
	return (int(alpha) << 24) + (int(rgb.r) << 16) + (int(rgb.g) << 8) + int(rgb.b);
}

uint32_t pack_colour(float alpha, Colour colour) {
	return (int(alpha) << 24) + (int(colour.red) << 16) + (int(colour.green) << 8) + int(colour.blue);
}

vector<uint32_t> pack_vector(vector<glm::vec3> rgbs) {
	vector<uint32_t> result;
	for (int i = 0; i < rgbs.size(); i++) {
		result.push_back(pack_vec3(255.0, rgbs[i]));
	}
	return result;
}

int getNumberOfValues(CanvasPoint a, CanvasPoint b) {
	float diffx = b.x - a.x;
	float diffy = b.y - a.y;
	float max_val = max(abs(diffx), abs(diffy));
	return round(max_val);
}

CanvasTriangle h_sort(CanvasTriangle triangle) {
	CanvasPoint vertices[3];
	vertices[0] = triangle.vertices[0];
	vertices[1] = triangle.vertices[1];
	vertices[2] = triangle.vertices[2];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			if (vertices[j].y > vertices[j + 1].y) {
				swap(vertices[j], vertices[j+1]);
			}
		}
	}
	return CanvasTriangle(vertices[0], vertices[1], vertices[2]);
}

void line(DrawingWindow &window, CanvasPoint a, CanvasPoint b, Colour colour) {
	int numberOfValues = getNumberOfValues(a, b);
	vector<CanvasPoint> points = interpolateCanvasPoint(a, b, numberOfValues);
	for (int i = 0; i < points.size(); i++) {
		window.setPixelColour((points[i].x), (points[i].y), pack_colour(255.0, colour));
	}
}

void strokedTriangle(DrawingWindow &window, CanvasTriangle triangle, Colour colour) {
	line(window, triangle.vertices[0], triangle.vertices[1], colour);
	line(window, triangle.vertices[1], triangle.vertices[2], colour);
	line(window, triangle.vertices[2], triangle.vertices[0], colour);
}

void filledTriangle(DrawingWindow &window, CanvasTriangle triangle, Colour colour) {
	CanvasTriangle sorted = h_sort(triangle);
	int numberOfValues = getNumberOfValues(sorted.vertices[0], sorted.vertices[2]);
	vector<CanvasPoint> top2bot = interpolateCanvasPoint(sorted.vertices[0], sorted.vertices[2], numberOfValues);
	CanvasPoint mid;

	float stepy = (sorted.vertices[2].y - sorted.vertices[0].y) / (numberOfValues - 1);
	int index = (sorted.vertices[1].y - sorted.vertices[0].y) / stepy;
	if (round(top2bot[index].y) == sorted.vertices[1].y) mid = CanvasPoint(round(top2bot[index].x), round(top2bot[index].y));
	else if (round(top2bot[index + 1].y) == sorted.vertices[1].y) {
		index++;
		mid = CanvasPoint(round(top2bot[index].x), round(top2bot[index].y));
	}
	else {
		index--;
		mid = CanvasPoint(round(top2bot[index].x), round(top2bot[index].y));
	}

	CanvasTriangle top = CanvasTriangle(sorted.vertices[0], mid, sorted.vertices[1]);
	CanvasTriangle bot = CanvasTriangle(sorted.vertices[1], mid, sorted.vertices[2]);

	int top_values = (top.vertices[1].y - top.vertices[0].y) + 1;
	int bot_values = (bot.vertices[2].y - top.vertices[1].y) + 1;

	vector<CanvasPoint> top1 = interpolateCanvasPoint(top.vertices[0], top.vertices[1], top_values);
	vector<CanvasPoint> top2 = interpolateCanvasPoint(top.vertices[0], top.vertices[2], top_values);

	vector<CanvasPoint> bot1 = interpolateCanvasPoint(bot.vertices[0], bot.vertices[2], bot_values);
	vector<CanvasPoint> bot2 = interpolateCanvasPoint(bot.vertices[1], bot.vertices[2], bot_values);

	//cout << top << endl;
	//cout << bot << endl;
	//cout << top_values << endl;
	//cout << bot_values << endl;

	for (int i = 0; i < top_values; i++) {
		if (round(top1[i].x) < round(top2[i].x)) {
			for (int j = round(top1[i].x); j < round(top2[i].x) + 1; j++) {
				window.setPixelColour(j, top1[i].y, pack_colour(255.0, colour));
			}
		}
		else {
			for (int j = round(top1[i].x); j > round(top2[i].x) - 1; j--) {
				window.setPixelColour(j, top1[i].y, pack_colour(255.0, colour));
			}
		}
	}
	for (int i = 0; i < bot_values; i++) {
	  if (round(bot1[i].x) < round(bot2[i].x)) {
	    for (int j = round(bot1[i].x); j < round(bot2[i].x) + 1; j++) {
	      window.setPixelColour(j, bot1[i].y, pack_colour(255.0, colour));
	    }
	  }
	  else {
	    for (int j = round(bot1[i].x); j > round(bot2[i].x) - 1; j--) {
	      window.setPixelColour(j, bot1[i].y, pack_colour(255.0, colour));
	    }
	  }
	}
	strokedTriangle(window, sorted, Colour(255, 255, 255));
}

void textureMapping(DrawingWindow &window, CanvasTriangle triangle) {
	TextureMap texture = TextureMap("/Users/kshitij/Projects/cg/CG2020/RedNoise/texture.ppm");
	CanvasTriangle sorted = h_sort(triangle);
	int numberOfValues = getNumberOfValues(sorted.vertices[0], sorted.vertices[2]);
	vector<CanvasPoint> top2bot = interpolateCanvasPoint(sorted.vertices[0], sorted.vertices[2], numberOfValues);
	vector<TexturePoint> texture_top2bot = interpolateTPoint(sorted.vertices[0].texturePoint, sorted.vertices[2].texturePoint, numberOfValues);
	CanvasPoint mid;
	TexturePoint texture_mid;

	float stepy = (sorted.vertices[2].y - sorted.vertices[0].y) / (numberOfValues - 1);
	int index = (sorted.vertices[1].y - sorted.vertices[0].y) / stepy;
	if (round(top2bot[index].y) == sorted.vertices[1].y) mid = CanvasPoint(round(top2bot[index].x), round(top2bot[index].y));
	else if (round(top2bot[index + 1].y) == sorted.vertices[1].y) {
		index++;
		mid = CanvasPoint(round(top2bot[index].x), round(top2bot[index].y));
	}
	else {
		index--;
		mid = CanvasPoint(round(top2bot[index].x), round(top2bot[index].y));
	}

	index = (sorted.vertices[1].y - sorted.vertices[0].y) / stepy;
	if (round(texture_top2bot[index].y) == sorted.vertices[1].texturePoint.y) texture_mid = TexturePoint(round(texture_top2bot[index].x), round(texture_top2bot[index].y));
	else if (round(texture_top2bot[index + 1].y) == sorted.vertices[1].texturePoint.y) {
		index++;
		texture_mid = TexturePoint(round(texture_top2bot[index].x), round(texture_top2bot[index].y));
	}
	else {
		index--;
		texture_mid = TexturePoint(round(texture_top2bot[index].x), round(texture_top2bot[index].y));
	}

	mid.texturePoint = texture_mid;
	cout << texture_mid << endl;

	CanvasTriangle top = CanvasTriangle(sorted.vertices[0], mid, sorted.vertices[1]);
	CanvasTriangle bot = CanvasTriangle(sorted.vertices[1], mid, sorted.vertices[2]);

	int top_values = (top.vertices[1].y - top.vertices[0].y) + 1;
	int bot_values = (bot.vertices[2].y - top.vertices[1].y) + 1;

	vector<CanvasPoint> top1 = interpolateCanvasPoint(top.vertices[0], top.vertices[1], top_values);
	vector<CanvasPoint> top2 = interpolateCanvasPoint(top.vertices[0], top.vertices[2], top_values);
	vector<TexturePoint> texture_top1 = interpolateTPoint(top.vertices[0].texturePoint, top.vertices[1].texturePoint, top_values);
	vector<TexturePoint> texture_top2 = interpolateTPoint(top.vertices[0].texturePoint, top.vertices[2].texturePoint, top_values);

	vector<CanvasPoint> bot1 = interpolateCanvasPoint(bot.vertices[0], bot.vertices[2], bot_values);
	vector<CanvasPoint> bot2 = interpolateCanvasPoint(bot.vertices[1], bot.vertices[2], bot_values);
	vector<TexturePoint> texture_bot1 = interpolateTPoint(bot.vertices[0].texturePoint, bot.vertices[2].texturePoint, bot_values);
	vector<TexturePoint> texture_bot2 = interpolateTPoint(bot.vertices[1].texturePoint, bot.vertices[2].texturePoint, bot_values);

	vector<TexturePoint> top_textureInterpolate;
	vector<TexturePoint> bot_textureInterpolate;
	uint32_t colour = 0;
	int top_stepx;
	int bot_stepx;

	for (int i = 0; i < top_values; i++) {
		top_stepx = (int) abs(round(top1[i].x) - round(top2[i].x)) + 1;
		if (texture_top1[i].x > texture_top2[i].x) top_textureInterpolate = interpolateTPoint(texture_top2[i], texture_top1[i], top_stepx);
		else top_textureInterpolate = interpolateTPoint(texture_top1[i], texture_top2[i], top_stepx);
		if (round(top1[i].x) < round(top2[i].x)) {
			for (int j = round(top1[i].x); j < round(top2[i].x) + 1; j++) {
				int tx = (int) round(top_textureInterpolate[j - round(top1[i].x)].x);
				int ty = (int) round(top_textureInterpolate[j - round(top1[i].x)].y);
				colour = texture.pixels[(ty * texture.width) + tx];
				window.setPixelColour(j, top1[i].y, colour);
			}
		}
		else {
			for (int j = round(top1[i].x); j > round(top2[i].x) - 1; j--) {
				int tx = (int) round(top_textureInterpolate[j - round(top2[i].x)].x);
				int ty = (int) round(top_textureInterpolate[j - round(top2[i].x)].y);
				colour = texture.pixels[(ty * texture.width) + tx];
				window.setPixelColour(j, top1[i].y, colour);
			}
		}
	}
	for (int i = 0; i < bot_values; i++) {
		bot_stepx = (int) abs(round(bot1[i].x) - round(bot2[i].x)) + 1;
		if (texture_bot1[i].x > texture_bot2[i].x) bot_textureInterpolate = interpolateTPoint(texture_bot2[i], texture_bot1[i], bot_stepx);
		else bot_textureInterpolate = interpolateTPoint(texture_bot1[i], texture_bot2[i], bot_stepx);
	  if (round(bot1[i].x) < round(bot2[i].x)) {
	    for (int j = round(bot1[i].x); j < round(bot2[i].x) + 1; j++) {
				int tx = (int) round(bot_textureInterpolate[j - round(bot1[i].x)].x);
				int ty = (int) round(bot_textureInterpolate[j - round(bot1[i].x)].y);
				colour = texture.pixels[(ty * texture.width) + tx];
	      window.setPixelColour(j, bot1[i].y, colour);
	    }
	  }
	  else {
	    for (int j = round(bot1[i].x); j > round(bot2[i].x) - 1; j--) {
				int tx = (int) round(bot_textureInterpolate[j - round(bot2[i].x)].x);
				int ty = (int) round(bot_textureInterpolate[j - round(bot2[i].x)].y);
				colour = texture.pixels[(ty * texture.width) + tx];
	      window.setPixelColour(j, bot1[i].y, colour);
	    }
	  }
	}
	strokedTriangle(window, sorted, Colour(255, 255, 255));
}

void draw(DrawingWindow &window) {
	window.clearPixels();
	for (size_t y = 0; y < window.height; y++) {
		for (size_t x = 0; x < window.width; x++) {
			float red = rand() % 256;
			float green = 0.0;
			float blue = 0.0;
			uint32_t colour = (255 << 24) + (int(red) << 16) + (int(green) << 8) + int(blue);
			window.setPixelColour(x, y, colour);
		}
	}
}

void update(DrawingWindow &window) {
	// Function for performing animation (shifting artifacts or moving the camera)
}

void handleEvent(SDL_Event event, DrawingWindow &window) {
	if (event.type == SDL_KEYDOWN) {
		if (event.key.keysym.sym == SDLK_LEFT) std::cout << "LEFT" << std::endl;
		else if (event.key.keysym.sym == SDLK_RIGHT) std::cout << "RIGHT" << std::endl;
		else if (event.key.keysym.sym == SDLK_UP) std::cout << "UP" << std::endl;
		else if (event.key.keysym.sym == SDLK_DOWN) std::cout << "DOWN" << std::endl;
		else if (event.key.keysym.sym == SDLK_l) {
			CanvasPoint a = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasPoint b = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			Colour colour = Colour(rand()%256, rand()%256, rand()%256);
			line(window, a, b, colour);
		}
		else if (event.key.keysym.sym == SDLK_u) {
			CanvasPoint a = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasPoint b = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasPoint c = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasTriangle t = CanvasTriangle(a, b, c);
			Colour colour = Colour(rand()%256, rand()%256, rand()%256);
			strokedTriangle(window, t, colour);
		}
		else if (event.key.keysym.sym == SDLK_f) {
			CanvasPoint a = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasPoint b = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasPoint c = CanvasPoint(rand()%WIDTH, rand()%HEIGHT);
			CanvasTriangle t = CanvasTriangle(a, b, c);
			Colour colour = Colour(rand()%256, rand()%256, rand()%256);
			filledTriangle(window, t, colour);
		}
		else if (event.key.keysym.sym == SDLK_t) {
			CanvasPoint a = CanvasPoint(160, 10);
			a.texturePoint = TexturePoint(195, 5);
			CanvasPoint b = CanvasPoint(300, 230);
			b.texturePoint = TexturePoint(395, 380);
			CanvasPoint c = CanvasPoint(10, 150);
			c.texturePoint = TexturePoint(65, 330);
			CanvasTriangle t = CanvasTriangle(a, b, c);
			textureMapping(window, t);
		}
	} else if (event.type == SDL_MOUSEBUTTONDOWN) window.savePPM("output.ppm");
}

int main(int argc, char *argv[]) {
	srand(time(0));
	DrawingWindow window = DrawingWindow(WIDTH, HEIGHT, false);
	SDL_Event event;
	while (true) {
		// We MUST poll for events - otherwise the window will freeze !
		if (window.pollForInputEvents(event)) handleEvent(event, window);
		update(window);
		// Need to render the frame at the end, or nothing actually gets shown on the screen !
		window.renderFrame();
	}
}
