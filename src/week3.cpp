#include <CanvasTriangle.h>
#include <DrawingWindow.h>
#include <Utils.h>
#include <fstream>
#include <vector>
#include <glm/glm.hpp>
#include <CanvasPoint.h>
#include <Colour.h>
#include <CanvasTriangle.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <TextureMap.h>
#include <TexturePoint.h>
#include <ModelTriangle.h>
#include <unordered_map>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <RayTriangleIntersection.h>
#include <algorithm>
#include <BumpPoint.h>

#define FACTOR 3
#define WIDTH 320*FACTOR
#define HEIGHT 240*FACTOR
using namespace std;

glm::vec3 camera;
glm::mat3 orient;
glm::vec3 light;
float focal_length;
float depth_buffer[HEIGHT][WIDTH];
TextureMap texture;
TextureMap bump;
vector<ModelTriangle> triangles;
float ambient = 0.2f;

CanvasTriangle v_sort(CanvasTriangle triangle);
CanvasTriangle pre_compute(CanvasTriangle triangle);
float edgeFunction(glm::vec3 a, glm::vec3 b, glm::vec3 c);
uint32_t pack_colour(float alpha, Colour colour);

float clamp(float a, float lo, float hi) {
	return max(lo, min(hi, a));
}

vector<float> interpolateSingleFloats(float from, float to, int numberOfValues) {
	float step = (to - from) / (numberOfValues - 1);
	vector<float> result;
	result.push_back(from);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(result[i - 1] + step);
	}
	return result;
}

vector<glm::vec3> interpolateThreeElementValues(glm::vec3 from, glm::vec3 to, int numberOfValues) {
	glm::vec3 step = glm::vec3(
		(to.x - from.x) / (numberOfValues - 1),
		(to.y - from.y) / (numberOfValues - 1),
		(to.z - from.z) / (numberOfValues - 1)
	);
	vector<glm::vec3> result;
	result.push_back(from);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(glm::vec3(result[i - 1].x + step.x, result[i - 1].y + step.y, result[i - 1].z + step.z));
	}
	return result;
}

vector<CanvasPoint> interpolateCanvasPoint(CanvasPoint a, CanvasPoint b, int numberOfValues) {
	glm::vec2 step = glm::vec2(
		(b.x - a.x) / (numberOfValues - 1),
		(b.y - a.y) / (numberOfValues - 1)
	);
	vector<CanvasPoint> result;
	result.push_back(a);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(CanvasPoint(result[i - 1].x + step.x, result[i - 1].y + step.y));
	}
	return result;
}

vector<TexturePoint> interpolateTPoint(TexturePoint a, TexturePoint b, int numberOfValues) {
	glm::vec2 step = glm::vec2(
		(b.x - a.x) / (numberOfValues - 1),
		(b.y - a.y) / (numberOfValues - 1)
	);
	vector<TexturePoint> result;
	result.push_back(a);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(TexturePoint(result[i - 1].x + step.x, result[i - 1].y + step.y));
	}
	return result;
}

void print_vec(glm::vec3 a) {
	cout << "(" << a.x << ", " << a.y << ", " << a.z << ")" << endl;
}

glm::vec3 get_point(RayTriangleIntersection intersect) {
	glm::vec3 p0 = intersect.intersectedTriangle.vertices[0];
	glm::vec3 p1 = intersect.intersectedTriangle.vertices[1];
	glm::vec3 p2 = intersect.intersectedTriangle.vertices[2];

	float u = intersect.intersectionPoint.y;
	float v = intersect.intersectionPoint.z;

	glm::vec3 point = p0 + u*(p1 - p0) + v*(p2 - p0);
	return point;
}

glm::vec3 get_refracted(glm::vec3 lightRay, glm::vec3 normal, float eta) {
	eta = 2.0f - eta;
	float cosi = glm::dot(lightRay, normal);
	glm::vec3 refractedRayDirection = (lightRay * eta - normal * (-cosi +eta * cosi));
	return refractedRayDirection;
}

CanvasTriangle modelToCanvasPoint(ModelTriangle triangle) {
	CanvasPoint points[3];
	float scale = WIDTH;
	for(int j = 0; j < 3; j++) {
		glm::vec3 a = glm::vec3(triangle.vertices[j].x, triangle.vertices[j].y, triangle.vertices[j].z);
		a = a - camera;
		a = a * orient;
		a.x = focal_length * (a.x / a.z);
		a.y = focal_length * (a.y / a.z);
		a.x = -a.x * scale;
		a.y = a.y * scale;
		a.x += WIDTH/2.0;
		a.y += HEIGHT/2.0;
		points[j] = CanvasPoint(a.x, a.y, a.z);
		if (triangle.texturePoints[j].x != 0 && triangle.texturePoints[j].y != 0) {
			float t_x = triangle.texturePoints[j].x;
			float t_y = triangle.texturePoints[j].y;
			t_x = round(texture.width * t_x);
			t_y = round(texture.height * t_y);
			points[j].texturePoint = TexturePoint(t_x, t_y);
		}
		if (triangle.bumpPoints[j].x != 0 && triangle.bumpPoints[j].y != 0) {
			float t_x = triangle.bumpPoints[j].x;
			float t_y = triangle.bumpPoints[j].y;
			t_x = round(bump.width * t_x);
			t_y = round(bump.height * t_y);
			points[j].texturePoint = TexturePoint(t_x, t_y);
		}
	}
	return (CanvasTriangle(points[0], points[1], points[2]));
}

RayTriangleIntersection getClosestIntersection(glm::vec3 rayDirection, glm::vec3 origin, int depth, int pixel_x, int pixel_y) {
	RayTriangleIntersection closest;
	closest.distanceFromCamera = INFINITY;
	closest.triangleIndex = -1;
	if (depth == 10) return closest;
	for (int i = 0; i < triangles.size(); i++) {
		ModelTriangle triangle = triangles[i];
		glm::vec3 e0 = triangle.vertices[1] - triangle.vertices[0];
		glm::vec3 e1 = triangle.vertices[2] - triangle.vertices[0];
		glm::vec3 SPVector = origin - triangle.vertices[0];
		glm::mat3 DEMatrix(-rayDirection, e0, e1);
		glm::vec3 possibleSolution = glm::inverse(DEMatrix) * SPVector;
		float t = possibleSolution.x;
		float u = possibleSolution.y;
		float v = possibleSolution.z;
		if (((u >= 0.0) && (u <= 1.0)) && ((v >= 0.0) && (v <= 1.0)) && (u + v <= 1.0) && (t > 0.0)) {
			if (t < closest.distanceFromCamera && t > 0.0000001) {
				closest.intersectionPoint = possibleSolution;
				closest.distanceFromCamera = t;
				closest.intersectedTriangle = triangle;
				closest.triangleIndex = i;
			}
		}
	}
	if (closest.intersectedTriangle.reflection) {
		glm::vec3 point = get_point(closest);
		glm::vec3 normal = closest.intersectedTriangle.normal;
		glm::vec3 reflectedRayDirection = rayDirection - (2.0f * glm::dot(rayDirection, normal) * normal);
		reflectedRayDirection = glm::normalize(reflectedRayDirection);
		glm::vec3 bias = point + (0.01f * normal);
		return getClosestIntersection(reflectedRayDirection, bias, depth + 1, pixel_x, pixel_y);
	}
	else if (closest.intersectedTriangle.ior_mat != 0.0f) {
		glm::vec3 point = get_point(closest);
		glm::vec3 normal = closest.intersectedTriangle.normal;
		bool outside = glm::dot(rayDirection, normal) < 0;
		float ior = outside ? closest.intersectedTriangle.ior_mat : (1.0 / closest.intersectedTriangle.ior_mat);
		glm::vec3 refractedRayDirection = get_refracted(rayDirection, normal, ior);
		glm::vec3 bias = outside ? point - (0.01f * normal) : point + (0.01f * normal);
		return getClosestIntersection(refractedRayDirection, bias, depth + 1, pixel_x, pixel_y);
	}
	else if (closest.intersectedTriangle.texturePoints[0].x != 0 && closest.intersectedTriangle.texturePoints[0].y != 0 && closest.triangleIndex != -1) {
		ModelTriangle t = closest.intersectedTriangle;
		CanvasTriangle triangle = modelToCanvasPoint(t);
		CanvasTriangle x = pre_compute(triangle);
		CanvasTriangle sorted = v_sort(x);
		if ((sorted.vertices[0].x < sorted.vertices[1].x || sorted.vertices[2].x - sorted.vertices[0].x < sorted.vertices[1].x - sorted.vertices[0].x)) {
			swap(sorted.vertices[0], sorted.vertices[1]);
		}
		glm::vec3 a = glm::vec3(sorted.vertices[0].x, sorted.vertices[0].y, sorted.vertices[0].depth);
		glm::vec3 b = glm::vec3(sorted.vertices[1].x, sorted.vertices[1].y, sorted.vertices[1].depth);
		glm::vec3 c = glm::vec3(sorted.vertices[2].x, sorted.vertices[2].y, sorted.vertices[2].depth);
		float area = edgeFunction(a, b, c);

		float npixel_x = pixel_x;// + 0.5;
		float npixel_y = pixel_y;// + 0.5;

		glm::vec3 pixel = glm::vec3(npixel_x, npixel_y, 0.0f);
		float w0 = edgeFunction(b, c, pixel);
		float w1 = edgeFunction(c, a, pixel);
		float w2 = edgeFunction(a, b, pixel);
		if (w0 >= 0.0 && w1 >= 0.0 && w2 >= 0.0) {
			w0 /= area;
			w1 /= area;
			w2 /= area;
			float z = (a.z * w0) + (b.z * w1) + (c.z * w2);
			float t_x = (sorted.vertices[0].texturePoint.x * w0) + (sorted.vertices[1].texturePoint.x * w1) + (sorted.vertices[2].texturePoint.x * w2);
			t_x = t_x * (1/z);
			float t_y = (sorted.vertices[0].texturePoint.y * w0) + (sorted.vertices[1].texturePoint.y * w1) + (sorted.vertices[2].texturePoint.y * w2);
			t_y = t_y * (1/z);
			uint32_t colour = texture.pixels[(round(t_y) * texture.width) + round(t_x)];
			int red = (colour & 0x00FF0000) >> 16;
			int green = (colour & 0x0000FF00) >> 8;
			int blue = (colour & 0x000000FF);
			closest.intersectedTriangle.colour = Colour(red, green, blue);
		}
	}
	float mult = pow(0.8, depth);
	int red = (int) round(closest.intersectedTriangle.colour.red * mult);
	int green = (int) round(closest.intersectedTriangle.colour.green * mult);
	int blue = (int) round(closest.intersectedTriangle.colour.blue * mult);
	closest.intersectedTriangle.colour = Colour(red, green, blue);
	return closest;
}

bool shadow(glm::vec3 point, glm::vec3 normal, int index) {
	glm::vec3 shadowRay = glm::normalize(light - point);
	float distance = glm::length(light - point);
	for (int i = 0; i < triangles.size(); i++) {
		ModelTriangle triangle = triangles[i];
		glm::vec3 e0 = triangle.vertices[1] - triangle.vertices[0];
		glm::vec3 e1 = triangle.vertices[2] - triangle.vertices[0];
		glm::vec3 SPVector = (point) - triangle.vertices[0];
		glm::mat3 DEMatrix(-shadowRay, e0, e1);
		glm::vec3 possibleSolution = glm::inverse(DEMatrix) * SPVector;
		float t = possibleSolution.x;
		float u = possibleSolution.y;
		float v = possibleSolution.z;
		if (((u >= 0.0) && (u <= 1.0)) && ((v >= 0.0) && (v <= 1.0)) && (u + v <= 1.0) && (t > 0.0) && (i != index)) {
			if (t < distance) {
				return true;
			}
		}
	}
	return false;
}

float proximity(glm::vec3 point) {
	float distance = glm::length(light - point);
	float proximity = 2 / (4 * M_PI * pow(distance, 2));
	return proximity;
}

float angle_of_incidence(glm::vec3 point, glm::vec3 normal) {
	glm::vec3 lightRay = glm::normalize(light - point);
	float angle_of_incidence = glm::dot(normal, lightRay);
	return angle_of_incidence;
}

float specular(glm::vec3 point, glm::vec3 normal, int gloss) {
	glm::vec3 lightRay = glm::normalize(point - light);
	glm::vec3 reflected = (2.0f * normal * glm::dot(normal, -lightRay)) + lightRay;
	reflected = glm::normalize(reflected);
	glm::vec3 view = glm::normalize(camera - point);
	float specular = glm::dot(reflected, view);
	specular = pow(specular, gloss);
	return specular;
}

float flat_shading(RayTriangleIntersection intersect) {
	glm::vec3 point = get_point(intersect);
	glm::vec3 normal = intersect.intersectedTriangle.normal;
	glm::vec3 bias = point + (0.001f * normal);
	if (shadow(bias, normal, intersect.triangleIndex)) return ambient;
	float diffuse = proximity(point) * angle_of_incidence(point, normal);
	return ambient + 0.1f + diffuse;
}

float gouraud_shading(RayTriangleIntersection intersect) {
	float u = intersect.intersectionPoint[1];
	float v = intersect.intersectionPoint[2];
	float w = 1.0f - (u + v);
	glm::vec3 point = get_point(intersect);
	glm::vec3 normal = intersect.intersectedTriangle.normal;
	glm::vec3 bias = point + (0.001f * normal);
	if (shadow(bias, normal, intersect.triangleIndex)) return ambient;
	float proximity_brightness = proximity(point);
	float brightness1 = specular(point, intersect.intersectedTriangle.vnormals[0], (int) intersect.intersectedTriangle.gloss) + (proximity_brightness * angle_of_incidence(point, intersect.intersectedTriangle.vnormals[0]));
	float brightness2 = specular(point, intersect.intersectedTriangle.vnormals[1], (int) intersect.intersectedTriangle.gloss) + (proximity_brightness * angle_of_incidence(point, intersect.intersectedTriangle.vnormals[1]));
	float brightness3 = specular(point, intersect.intersectedTriangle.vnormals[2], (int) intersect.intersectedTriangle.gloss) + (proximity_brightness * angle_of_incidence(point, intersect.intersectedTriangle.vnormals[2]));
	return ((w * brightness1) + (u * brightness2) + (v * brightness3));
}

float phong_shading(RayTriangleIntersection intersect, int pixel_x, int pixel_y) {
	float u = intersect.intersectionPoint[1];
	float v = intersect.intersectionPoint[2];
	float w = 1.0f - (u + v);
	glm::vec3 normal;
	if (intersect.intersectedTriangle.bumpPoints[0].x != 0 && intersect.intersectedTriangle.bumpPoints[0].y != 0 && intersect.triangleIndex != -1) {
		ModelTriangle t = intersect.intersectedTriangle;
		CanvasTriangle triangle = modelToCanvasPoint(t);
		CanvasTriangle x = pre_compute(triangle);
		CanvasTriangle sorted = v_sort(x);
		if ((sorted.vertices[0].x < sorted.vertices[1].x || sorted.vertices[2].x - sorted.vertices[0].x < sorted.vertices[1].x - sorted.vertices[0].x)) {
			swap(sorted.vertices[0], sorted.vertices[1]);
		}
		glm::vec3 a = glm::vec3(sorted.vertices[0].x, sorted.vertices[0].y, sorted.vertices[0].depth);
		glm::vec3 b = glm::vec3(sorted.vertices[1].x, sorted.vertices[1].y, sorted.vertices[1].depth);
		glm::vec3 c = glm::vec3(sorted.vertices[2].x, sorted.vertices[2].y, sorted.vertices[2].depth);
		float area = edgeFunction(a, b, c);

		float npixel_x = pixel_x;// + 0.5;
		float npixel_y = pixel_y;// + 0.5;

		glm::vec3 pixel = glm::vec3(npixel_x, npixel_y, 0.0f);
		float w0 = edgeFunction(b, c, pixel);
		float w1 = edgeFunction(c, a, pixel);
		float w2 = edgeFunction(a, b, pixel);
		if (w0 >= 0.0 && w1 >= 0.0 && w2 >= 0.0) {
			w0 /= area;
			w1 /= area;
			w2 /= area;
			float z = (a.z * w0) + (b.z * w1) + (c.z * w2);
			float t_x = (sorted.vertices[0].texturePoint.x * w0) + (sorted.vertices[1].texturePoint.x * w1) + (sorted.vertices[2].texturePoint.x * w2);
			t_x = t_x * (1/z);
			float t_y = (sorted.vertices[0].texturePoint.y * w0) + (sorted.vertices[1].texturePoint.y * w1) + (sorted.vertices[2].texturePoint.y * w2);
			t_y = t_y * (1/z);
			uint32_t colour = bump.pixels[(round(t_y) * bump.width) + round(t_x)];
			int red = (colour & 0x00FF0000) >> 16;
			int green = (colour & 0x0000FF00) >> 8;
			int blue = (colour & 0x000000FF);
			normal = glm::vec3(red, green, blue);
		}
	}
	else normal = (w * intersect.intersectedTriangle.vnormals[0]) + (u * intersect.intersectedTriangle.vnormals[1]) + (v * intersect.intersectedTriangle.vnormals[2]);
	normal = glm::normalize(normal);
	glm::vec3 point = get_point(intersect);
	glm::vec3 bias = point + (0.001f * normal);
	if (shadow(bias, normal, intersect.triangleIndex)) return ambient;
	float diffuse = proximity(point) * angle_of_incidence(point, normal);
	float spec = specular(point, normal, (int) intersect.intersectedTriangle.gloss);
	return ambient + (diffuse * (1 - spec)) + spec;
}

CanvasPoint line_eq(CanvasPoint a, CanvasPoint b, float y) {
	glm::vec3 x1 = glm::vec3(a.x, a.y, 1/a.depth);
	glm::vec3 x2 = glm::vec3(b.x, b.y, 1/b.depth);
	glm::vec3 m = x2 - x1;
	float t;
	if (m.y == 0) {
		t = y - x1.x;
		t = t / m.x;
	}
	else {
		t = y - x1.y;
		t = t / m.y;
	}
	CanvasPoint result = CanvasPoint(((x1.x + (t * m.x))), (y), 1/(x1.z + (t * m.z)));
	return result;
}

uint32_t pack(float alpha, float red, float green, float blue) {
	return (int(alpha) << 24) + (int(red) << 16) + (int(green) << 8) + int(blue);
}

uint32_t pack_vec3(float alpha, glm::vec3 rgb) {
	return (int(alpha) << 24) + (int(rgb.r) << 16) + (int(rgb.g) << 8) + int(rgb.b);
}

uint32_t pack_colour(float alpha, Colour colour) {
	return (int(alpha) << 24) + (int(colour.red) << 16) + (int(colour.green) << 8) + int(colour.blue);
}

uint32_t pack_colour_proximity(float alpha, Colour colour, float scale) {
	return (int(alpha) << 24) + (int(colour.red * scale) << 16) + (int(colour.green * scale) << 8) + int(colour.blue * scale);
}

vector<uint32_t> pack_vector(vector<glm::vec3> rgbs) {
	vector<uint32_t> result;
	for (int i = 0; i < rgbs.size(); i++) {
		result.push_back(pack_vec3(255.0, rgbs[i]));
	}
	return result;
}

int getNumberOfValues(CanvasPoint a, CanvasPoint b) {
	float diffx = b.x - a.x;
	float diffy = b.y - a.y;
	float max_val = max(abs(diffx), abs(diffy));
	return round(max_val) + 1;
}

CanvasTriangle v_sort(CanvasTriangle triangle) {
	CanvasPoint vertices[3];
	vertices[0] = triangle.vertices[0];
	vertices[1] = triangle.vertices[1];
	vertices[2] = triangle.vertices[2];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			if (vertices[j].y > vertices[j + 1].y) {
				swap(vertices[j], vertices[j+1]);
			}
		}
	}
	return CanvasTriangle(vertices[0], vertices[1], vertices[2]);
}

bool compare_points(CanvasPoint a, CanvasTriangle t) {
	for (int i = 0; i < 3; i++) {
		if ((fabs(t.vertices[i].x - a.x) < 0.001) && (fabs(t.vertices[i].y - a.y) < 0.001)) return true;
	}
	return false;
}

void init_buffer() {
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x++) {
			depth_buffer[y][x] = 0.0;
		}
	}
}

bool onScreen(CanvasPoint a) {
	if ((round(a.x) < 0) || (round(a.x) >= WIDTH)) return false;
	else if ((round(a.y) < 0) || (round(a.y) >= HEIGHT)) return false;
	else return true;
}

void rotate_x(float angle) {
	glm::mat3 rx = glm::mat3(
		1.0, 0.0, 0.0,
		0.0, cos(angle * M_PI/180.0), sin(angle * M_PI/180.0),
		0.0, (-1 * sin(angle * M_PI/180.0)), cos(angle * M_PI/180.0)
	);
	camera = camera * rx;
}

void rotate_y(float angle) {
	glm::mat3 ry = glm::mat3(
		cos(angle * M_PI/180.0), 0.0, (-1 * sin(angle * M_PI/180.0)),
		0.0, 1.0, 0.0,
		sin(angle * M_PI/180.0), 0.0, cos(angle * M_PI/180.0)
	);
	camera = camera * ry;
}

void orient_x(float angle) {
	glm::mat3 rx = glm::mat3(
		1.0, 0.0, 0.0,
		0.0, cos(angle * M_PI/180.0), sin(angle * M_PI/180.0),
		0.0, (-1 * sin(angle * M_PI/180.0)), cos(angle * M_PI/180.0)
	);
	orient = orient * rx;
}

void orient_y(float angle) {
	glm::mat3 ry = glm::mat3(
		cos(angle * M_PI/180.0), 0.0, (-1 * sin(angle * M_PI/180.0)),
		0.0, 1.0, 0.0,
		sin(angle * M_PI/180.0), 0.0, cos(angle * M_PI/180.0)
	);
	orient = orient * ry;
}

void old_line(DrawingWindow &window, CanvasPoint a, CanvasPoint b, Colour colour) {
	int numberOfValues = getNumberOfValues(a, b);
	vector<CanvasPoint> points = interpolateCanvasPoint(a, b, numberOfValues);
	for (int i = 0; i < points.size(); i++) {
		window.setPixelColour((points[i].x), (points[i].y), pack_colour(255.0, colour));
	}
}

void line(DrawingWindow &window, CanvasPoint a, CanvasPoint b, Colour colour) {
	if (a.x == b.x && a.y == b.y) {
		if (onScreen(a)) {
			if (-1/a.depth > depth_buffer[(int)round(a.y)][(int)round(a.x)]) {
				depth_buffer[(int)round(a.y)][(int)round(a.x)] = -1/a.depth;
	 			window.setPixelColour(round(a.x), round(a.y), pack_colour(255.0, colour));
			}
		}
	}
	else {
		int numberOfValues = getNumberOfValues(a, b);
		vector<CanvasPoint> points = interpolateCanvasPoint(a, b, numberOfValues + 1);
		for (int i = 0; i < points.size(); i++) {
			CanvasPoint temp = line_eq(a, b, (points[i].x));
			float z = temp.depth;
			if (onScreen(points[i])) {
				if (-1/z > depth_buffer[(int)round(points[i].y)][(int)round(points[i].x)]) {
					depth_buffer[(int)round(points[i].y)][(int)round(points[i].x)] = -1/z;
					window.setPixelColour(round(points[i].x), round(points[i].y), pack_colour(255.0, colour));
				}
				else {
					if (isnan(z)) cout << "colour: " << temp << endl;
				}
			}
		}
	}
}

void texture_line(DrawingWindow &window, CanvasPoint a, CanvasPoint b) {
	if (a.x == b.x && a.y == b.y) {
		if (onScreen(a)) {
			if (-1/a.depth >= depth_buffer[(int)round(a.y)][(int)round(a.x)]) {
				depth_buffer[(int)round(a.y)][(int)round(a.x)] = -1/a.depth;
				window.setPixelColour(round(a.x), round(a.y), texture.pixels[(round(a.texturePoint.y) * texture.width) + a.texturePoint.x]);
			}
		}
	}
	else {
		int numberOfValues = getNumberOfValues(a, b);
		vector<CanvasPoint> points = interpolateCanvasPoint(a, b, numberOfValues + 1);
		vector<TexturePoint> t_points = interpolateTPoint(a.texturePoint, b.texturePoint, numberOfValues + 1);
		for (int i = 0; i < points.size(); i++) {
			CanvasPoint temp = line_eq(a, b, (points[i].x));
			float z = temp.depth;
			if (onScreen(points[i])) {
				if (-1/z >= depth_buffer[(int)round(points[i].y)][(int)round(points[i].x)]) {
					depth_buffer[(int)round(points[i].y)][(int)round(points[i].x)] = -1/z;
					window.setPixelColour(round(points[i].x), round(points[i].y), texture.pixels[(round(t_points[i].y) * texture.width) + round(t_points[i].x)]);
				}
				else {
					if (isnan(z)) cout << "texture: " << temp << endl;
				}
			}
		}
	}
}

void strokedTriangle(DrawingWindow &window, CanvasTriangle triangle, Colour colour) {
	old_line(window, triangle.vertices[0], triangle.vertices[1], colour);
	old_line(window, triangle.vertices[1], triangle.vertices[2], colour);
	old_line(window, triangle.vertices[2], triangle.vertices[0], colour);
}

void filledTriangle(DrawingWindow &window, CanvasTriangle triangle, Colour colour) {
	CanvasTriangle sorted = v_sort(triangle);
	CanvasPoint mid = line_eq(sorted.vertices[0], sorted.vertices[2], sorted.vertices[1].y);
	line(window, sorted.vertices[0], sorted.vertices[0], colour);
	for (int i = sorted.vertices[0].y + 1; i < sorted.vertices[2].y; i++) {
		if (i < mid.y) {
			CanvasPoint a1 = line_eq(sorted.vertices[0], sorted.vertices[2], i);
			CanvasPoint a2 = line_eq(sorted.vertices[0], sorted.vertices[1], i);
			line(window, a1, a2, colour);
		}
		else if (i == mid.y) {
			line(window, mid, sorted.vertices[1], colour);
		}
		else if (i > mid.y) {
			CanvasPoint a1 = line_eq(sorted.vertices[0], sorted.vertices[2], i);
			CanvasPoint a2 = line_eq(sorted.vertices[1], sorted.vertices[2], i);
			line(window, a1, a2, colour);
		}
	}
	line(window, sorted.vertices[2], sorted.vertices[2], colour);
	//strokedTriangle(window, sorted, Colour(255, 255, 255));
}

void textureMapping(DrawingWindow &window, CanvasTriangle triangle) {
	CanvasTriangle sorted = v_sort(triangle);
	CanvasPoint mid = line_eq(sorted.vertices[0], sorted.vertices[2], sorted.vertices[1].y);
	if (compare_points(mid, sorted)) {
		if (sorted.vertices[0].y != sorted.vertices[1].y) {
			int numberOfValues = round(sorted.vertices[1].y - sorted.vertices[0].y) + 1;
 			vector<CanvasPoint> left = interpolateCanvasPoint(sorted.vertices[0], sorted.vertices[1], numberOfValues + 1);
			vector<CanvasPoint> right = interpolateCanvasPoint(sorted.vertices[0], sorted.vertices[2], numberOfValues + 1);
			vector<TexturePoint> t_left = interpolateTPoint(sorted.vertices[0].texturePoint, sorted.vertices[1].texturePoint, numberOfValues + 1);
			vector<TexturePoint> t_right = interpolateTPoint(sorted.vertices[0].texturePoint, sorted.vertices[2].texturePoint, numberOfValues + 1);
			for (int i = 0; i < numberOfValues + 1; i++) {
				left[i].texturePoint = t_left[i];
				right[i].texturePoint = t_right[i];
				left[i].depth = line_eq(sorted.vertices[0], sorted.vertices[1], left[i].y).depth;
				right[i].depth = line_eq(sorted.vertices[0], sorted.vertices[2], right[i].y).depth;
				texture_line(window, left[i], right[i]);
			}
		}
		else {
			int numberOfValues = round(sorted.vertices[2].y - sorted.vertices[0].y) + 1;
			vector<CanvasPoint> left = interpolateCanvasPoint(sorted.vertices[0], sorted.vertices[2], numberOfValues + 1);
			vector<CanvasPoint> right = interpolateCanvasPoint(sorted.vertices[1], sorted.vertices[2], numberOfValues + 1);
			vector<TexturePoint> t_left = interpolateTPoint(sorted.vertices[0].texturePoint, sorted.vertices[2].texturePoint, numberOfValues + 1);
			vector<TexturePoint> t_right = interpolateTPoint(sorted.vertices[1].texturePoint, sorted.vertices[2].texturePoint, numberOfValues + 1);
			for (int i = 0; i < numberOfValues + 1; i++) {
				left[i].texturePoint = t_left[i];
				right[i].texturePoint = t_right[i];
				left[i].depth = line_eq(sorted.vertices[0], sorted.vertices[2], left[i].y).depth;
				right[i].depth = line_eq(sorted.vertices[1], sorted.vertices[2], right[i].y).depth;
				texture_line(window, left[i], right[i]);
			}
		}
	}
	else {
		int numberOfValues = round(sorted.vertices[2].y - sorted.vertices[0].y) + 1;
		float index = (mid.y - sorted.vertices[0].y);
		index = index / ((sorted.vertices[2].y - sorted.vertices[0].y + 1) / numberOfValues);
		index = round(index);
		vector<TexturePoint> t = interpolateTPoint(sorted.vertices[0].texturePoint, sorted.vertices[2].texturePoint, numberOfValues);
		TexturePoint t_mid = t[index];
		mid.texturePoint = t_mid;

		CanvasTriangle top = CanvasTriangle(sorted.vertices[0], mid, sorted.vertices[1]);
		CanvasTriangle bot = CanvasTriangle(sorted.vertices[1], mid, sorted.vertices[2]);

		int top_values = round(top.vertices[1].y - top.vertices[0].y) + 1;
		int bot_values = round(bot.vertices[2].y - top.vertices[1].y) + 1;

		vector<CanvasPoint> top_left = interpolateCanvasPoint(top.vertices[0], top.vertices[1], top_values + 1);
		vector<CanvasPoint> top_right = interpolateCanvasPoint(top.vertices[0], top.vertices[2], top_values + 1);
		vector<TexturePoint> ttop_left = interpolateTPoint(top.vertices[0].texturePoint, top.vertices[1].texturePoint, top_values + 1);
		vector<TexturePoint> ttop_right = interpolateTPoint(top.vertices[0].texturePoint, top.vertices[2].texturePoint, top_values + 1);

		vector<CanvasPoint> bot_left = interpolateCanvasPoint(bot.vertices[0], bot.vertices[2], bot_values + 1);
		vector<CanvasPoint> bot_right = interpolateCanvasPoint(bot.vertices[1], bot.vertices[2], bot_values + 1);
		vector<TexturePoint> tbot_left = interpolateTPoint(bot.vertices[0].texturePoint, bot.vertices[2].texturePoint, bot_values + 1);
		vector<TexturePoint> tbot_right = interpolateTPoint(bot.vertices[1].texturePoint, bot.vertices[2].texturePoint, bot_values + 1);

		for (int i = 0; i < top_values + 1; i++) {
			top_left[i].texturePoint = ttop_left[i];
			top_right[i].texturePoint = ttop_right[i];
			top_left[i].depth = line_eq(top.vertices[0], top.vertices[1], top_left[i].y).depth;
			top_right[i].depth = line_eq(top.vertices[0], top.vertices[2], top_right[i].y).depth;
			texture_line(window, top_left[i], top_right[i]);
		}

		for (int i = 0; i < bot_values + 1; i++) {
			bot_left[i].texturePoint = tbot_left[i];
			bot_right[i].texturePoint = tbot_right[i];
			bot_left[i].depth = line_eq(bot.vertices[0], bot.vertices[2], bot_left[i].y).depth;
			bot_right[i].depth = line_eq(bot.vertices[1], bot.vertices[2], bot_right[i].y).depth;
			texture_line(window, bot_left[i], bot_right[i]);
		}
	}
}

float edgeFunction(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
	return (c[0] - a[0]) * (b[1] - a[1]) - (c[1] - a[1]) * (b[0] - a[0]);
}

CanvasTriangle pre_compute(CanvasTriangle triangle) {
	CanvasTriangle result = triangle;
	for (int i = 0; i < 3; i++) {
		result.vertices[i].texturePoint.x /= -result.vertices[i].depth;
		result.vertices[i].texturePoint.y /= -result.vertices[i].depth;
		result.vertices[i].depth = -1 / result.vertices[i].depth;
	}
	return result;
}

int min3(float a, float b, float c) {
	return (min(a, min(b, c)));
}

int max3(float a, float b, float c) {
	return (max(a, max(b, c)));
}

void perspectiveMapping(DrawingWindow &window, CanvasTriangle triangle) {
	CanvasTriangle x = pre_compute(triangle);
	CanvasTriangle sorted = v_sort(x);
	if ((sorted.vertices[0].x < sorted.vertices[1].x || sorted.vertices[2].x - sorted.vertices[0].x < sorted.vertices[1].x - sorted.vertices[0].x)) {
		swap(sorted.vertices[0], sorted.vertices[1]);
	}
	glm::vec3 a = glm::vec3(sorted.vertices[0].x, sorted.vertices[0].y, sorted.vertices[0].depth);
	glm::vec3 b = glm::vec3(sorted.vertices[1].x, sorted.vertices[1].y, sorted.vertices[1].depth);
	glm::vec3 c = glm::vec3(sorted.vertices[2].x, sorted.vertices[2].y, sorted.vertices[2].depth);

	int xmin = min3(sorted.vertices[0].x, sorted.vertices[1].x, sorted.vertices[2].x);
	int xmax = max3(sorted.vertices[0].x, sorted.vertices[1].x, sorted.vertices[2].x);
	int ymin = min3(sorted.vertices[0].y, sorted.vertices[1].y, sorted.vertices[2].y);;
	int ymax = max3(sorted.vertices[0].y, sorted.vertices[1].y, sorted.vertices[2].y);
	float area = edgeFunction(a, b, c);
	int x0 = max(0, min(xmin, WIDTH));
	int x1 = min(WIDTH, max(0, xmax));
	int y0 = max(0, min(ymin, HEIGHT));
	int y1 = min(HEIGHT, max(0, ymax));
	for (int y = y0; y < y1; y++) {
		for (int x = x0; x < x1; x++) {
			glm::vec3 pixel = glm::vec3(x, y, 0.0f);
			float w0 = edgeFunction(b, c, pixel);
			float w1 = edgeFunction(c, a, pixel);
			float w2 = edgeFunction(a, b, pixel);
			if (w0 >= 0.0 && w1 >= 0.0 && w2 >= 0.0) {
				w0 /= area;
				w1 /= area;
				w2 /= area;
				float z = (a.z * w0) + (b.z * w1) + (c.z * w2);
				if (z > depth_buffer[y][x]) {
					depth_buffer[y][x] = z;
					float t_x = (sorted.vertices[0].texturePoint.x * w0) + (sorted.vertices[1].texturePoint.x * w1) + (sorted.vertices[2].texturePoint.x * w2);
					t_x = t_x * (1/z);
					float t_y = (sorted.vertices[0].texturePoint.y * w0) + (sorted.vertices[1].texturePoint.y * w1) + (sorted.vertices[2].texturePoint.y * w2);
					t_y = t_y * (1/z);
					window.setPixelColour(x, y, texture.pixels[(round(t_y) * texture.width) + round(t_x)]);
				}
			}
		}
	}
}

unordered_map<string, Colour> loadMTL(string mtllib) {
	ifstream file(mtllib);
	string str;
	unordered_map<string, Colour> colours;
	vector<string> temp;
	string name;
	Colour colour;
	while(getline(file, str)) {
		temp = split(str, ' ');
		if (temp[0] == "newmtl") name = temp[1];
		if (temp[0] == "Kd") {
			colour = Colour(name, (int) (255 * stof(temp[1])), (int) (255 * stof(temp[2])), (int) (255 * stof(temp[3])));
			colours[name] = colour;
		}
		if (temp[0] == "map_Kd") {
			texture = TextureMap(temp[1]);
		}
		if (temp[0] == "map_bump") {
			bump = TextureMap(temp[1]);
		}
	}
	file.close();
	return colours;
}

vector<CanvasTriangle> modelToCanvasPoints(vector<ModelTriangle> triangles) {
	vector<CanvasTriangle> result;
	CanvasPoint points[3];
	float scale = WIDTH;
	for (int i = 0; i < triangles.size(); i++) {
		for(int j = 0; j < 3; j++) {
			ModelTriangle triangle = triangles[i];
			glm::vec3 a = glm::vec3(triangle.vertices[j].x, triangle.vertices[j].y, triangle.vertices[j].z);
			a = a - camera;
			a = a * orient;
			a.x = focal_length * (a.x / a.z);
			a.y = focal_length * (a.y / a.z);
			a.x = -a.x * scale;
			a.y = a.y * scale;
			a.x += WIDTH/2.0;
			a.y += HEIGHT/2.0;
			points[j] = CanvasPoint(a.x, a.y, a.z);
			if (triangle.texturePoints[j].x != 0 && triangle.texturePoints[j].y != 0) {
				float t_x = triangle.texturePoints[j].x;
				float t_y = triangle.texturePoints[j].y;
				t_x = round(texture.width * t_x);
				t_y = round(texture.height * t_y);
				points[j].texturePoint = TexturePoint(t_x, t_y);
			}
		}
		result.push_back(CanvasTriangle(points[0], points[1], points[2]));
	}
	return result;
}

glm::vec3 worldSpacePosition(glm::vec2 pixel) {
	glm::vec3 cameraPixel;
	cameraPixel.x = (pixel.x - (WIDTH/2.0)) / (WIDTH);
	cameraPixel.y = -(pixel.y - (HEIGHT/2.0)) / (WIDTH);
	cameraPixel = glm::vec3(cameraPixel.x, cameraPixel.y, -focal_length);
	glm::vec3 worldPixel;
	worldPixel = (cameraPixel * glm::inverse(orient)) + camera;
	return worldPixel;
}

void set_light() {
	glm::vec3 sum = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 count = glm::vec3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < triangles.size(); i++) {
		if(triangles[i].name == "light") {
			for (int j = 0; j < 3; j++) {
				sum += triangles[i].vertices[j];
				count += glm::vec3(1.0f, 1.0f, 1.0f);
			}
		}
		else if (triangles[i].name == "short_box") {
			triangles[i].ior_mat = 1.5f;
		}
	}
	light = sum/count;
	triangles[26].reflection = true;
	triangles[31].reflection = true;
}

void set_normal() {
	for (int i = 0; i < triangles.size(); i++) {
		glm::vec3 a = triangles[i].vertices[1] - triangles[i].vertices[0];
		glm::vec3 b = triangles[i].vertices[2] - triangles[i].vertices[0];
		triangles[i].normal = glm::normalize(glm::cross(a, b));
	}
}

void set_vnormal() {
	for (int i = 0; i < triangles.size(); i++) {
		if (triangles[i].vnormals[0] == glm::vec3(0,0,0)) {
			std::array<glm::vec3, 3> normals;
			for (int j = 0; j < 3; j++) {
				vector<glm::vec3> added;
				normals[j] = triangles[i].normal;
				added.push_back(triangles[i].normal);
				int count = 1;
				for (int k = 0; k < triangles.size(); k++) {
					for (int l = 0; l < 3; l++) {
						if ((i != k) && (triangles[i].vertices[j] == triangles[k].vertices[l])) {
							if (find(added.begin(), added.end(), triangles[k].normal) != added.end()) {
								normals[j] = normals[j] + triangles[k].normal;
								count = count + 1;
								added.push_back(triangles[k].normal);
							}
						}
					}
				}
				normals[j] = glm::normalize(normals[j] / float(count));
			}
			triangles[i].vnormals = normals;
		}
	}
}

void loadOBJ(string name, float scale) {
	ifstream in(name);
	string str;
	vector<glm::vec3> vertices;
	vector<glm::vec3> vnormals;
	vector<glm::vec2> t_vertices;
	vector<glm::vec2> b_vertices;
	string mtllib;
	getline(in, str);
	if (split(str, ' ')[0] == "mtllib") mtllib = split(str, ' ')[1];
	unordered_map<string, Colour> colours = loadMTL(mtllib);
	Colour colour = colours["Red"];
	vector<string> temp;
	string object_name = "";
	while(getline(in, str)) {
		temp = split(str, ' ');
		if (temp[0] == "usemtl") colour = colours[temp[1]];
		if (temp[0] == "o") object_name = temp[1];
		if (temp[0] == "vn") vnormals.push_back(glm::vec3((stof(temp[1])), (stof(temp[2])), (stof(temp[3]))));
		if (temp[0] == "v") vertices.push_back(glm::vec3((scale * stof(temp[1])), (scale * stof(temp[2])), (scale * stof(temp[3]))));
		if (temp[0] == "vt") t_vertices.push_back(glm::vec2(stof(temp[1]), stof(temp[2])));
		if (temp[0] == "vb") b_vertices.push_back(glm::vec2(stof(temp[1]), stof(temp[2])));
		if (temp[0] == "f") {
			int a, b, c;
			int t_a = 0, t_b = 0, t_c = 0;
			int n_a = 0, n_b = 0, n_c = 0;
			int b_a = 0, b_b = 0, b_c = 0;
			a = stoi(split(temp[1], '/')[0]);
			b = stoi(split(temp[2], '/')[0]);
			c = stoi(split(temp[3], '/')[0]);
			if ((split(temp[1], '/')[1]) != "") {
				t_a = stoi(split(temp[1], '/')[1]);
				t_b = stoi(split(temp[2], '/')[1]);
				t_c = stoi(split(temp[3], '/')[1]);
			}
			if (count(temp[1].begin(), temp[1].end(), '/') == 2) {
				n_a = stoi(split(temp[1], '/')[2]);
				n_b = stoi(split(temp[2], '/')[2]);
				n_c = stoi(split(temp[3], '/')[2]);
			}
			if (count(temp[1].begin(), temp[1].end(), '/') == 3) {
				b_a = stoi(split(temp[1], '/')[3]);
				b_b = stoi(split(temp[2], '/')[3]);
				b_c = stoi(split(temp[3], '/')[3]);
			}
			if (object_name == "sphere") {
				triangles.push_back(ModelTriangle(object_name, vertices[a - 1], vertices[b - 1], vertices[c - 1], colour));
				triangles.back().gloss = 256.0f;
			}
			else if (object_name != "") triangles.push_back(ModelTriangle(object_name, vertices[a - 1], vertices[b - 1], vertices[c - 1], colour));
			else triangles.push_back(ModelTriangle(vertices[a - 1], vertices[b - 1], vertices[c - 1], colour));
			if (t_a) {
				triangles.back().texturePoints[0] = TexturePoint(t_vertices[t_a - 1].x, t_vertices[t_a - 1].y);
				triangles.back().texturePoints[1] = TexturePoint(t_vertices[t_b - 1].x, t_vertices[t_b - 1].y);
				triangles.back().texturePoints[2] = TexturePoint(t_vertices[t_c - 1].x, t_vertices[t_c - 1].y);
			}
			if (n_a) {
				triangles.back().vnormals[0] = glm::normalize(vnormals[n_a - 1]);
				triangles.back().vnormals[1] = glm::normalize(vnormals[n_b - 1]);
				triangles.back().vnormals[2] = glm::normalize(vnormals[n_c - 1]);
			}
			if (b_a) {
				triangles.back().bumpPoints[0] = BumpPoint(b_vertices[b_a - 1].x, b_vertices[b_a - 1].y);
				triangles.back().bumpPoints[1] = BumpPoint(b_vertices[b_b - 1].x, b_vertices[b_b - 1].y);
				triangles.back().bumpPoints[2] = BumpPoint(b_vertices[b_c - 1].x, b_vertices[b_c - 1].y);
			}
		}
	}
	in.close();
	camera = glm::vec3(0.0, 0.0, 4.0);
	orient = glm::mat3(
										 1.0, 0.0, 0.0,
										 0.0, 1.0, 0.0,
										 0.0, 0.0, 1.0
										);
	focal_length = 2.0;
	//light = glm::vec3(0, 0.5, 0.5);
	set_light();
	light.y -= 0.2;
	light.z += 0.3;
}

void wireframe(DrawingWindow &window) {
	init_buffer();
	vector<CanvasTriangle> a = modelToCanvasPoints(triangles);
	for (int i = 0; i < a.size(); i++) {
		strokedTriangle(window, a[i], triangles[i].colour);
	}
}

void raster(DrawingWindow &window) {
	init_buffer();
	vector<CanvasTriangle> a = modelToCanvasPoints(triangles);
	for (int i = 0; i < a.size(); i++) {
		if (a[i].vertices[0].texturePoint.x != -1) perspectiveMapping(window, a[i]);
		else filledTriangle(window, a[i], triangles[i].colour);
	}
}

void raytrace(DrawingWindow &window) {
	for (size_t y = 0; y < window.height; y++) {
		old_line(window, CanvasPoint(0, y), CanvasPoint(WIDTH-1, y), Colour(255, 255, 255));
		window.renderFrame();
		for (size_t x = 0; x < window.width; x++) {
			glm::vec3 worldPixel = worldSpacePosition(glm::vec2(x, y));
			glm::vec3 rayDirection = glm::normalize(worldPixel - camera);
			RayTriangleIntersection intersect = getClosestIntersection(rayDirection, camera, 0, x, y);
			if (intersect.triangleIndex != -1) {
				float brightness = phong_shading(intersect, x, y);
				brightness = clamp(brightness, ambient, 1.0f);
				window.setPixelColour(x, y, pack_colour_proximity(255.0, intersect.intersectedTriangle.colour, brightness));
			}
			else {
				window.setPixelColour(x, y, pack_colour(255.0, Colour(0, 0, 0)));
			}
		}
		window.renderFrame();
	}
	float scale = WIDTH;
	glm::vec3 a = light;
	a = a - camera;
	a = a * orient;
	a.x = focal_length * (a.x / a.z);
	a.y = focal_length * (a.y / a.z);
	a.x = -a.x * scale;
	a.y = a.y * scale;
	a.x += WIDTH/2.0;
	a.y += HEIGHT/2.0;
	if (a.x >= 0 && a.y >= 0 && a.x < WIDTH && a.y < HEIGHT) window.setPixelColour(a.x, a.y, pack_colour(255.0, Colour(255, 255, 255)));
}

void lookat(CanvasPoint a) {
	glm::vec3 vec_a = glm::vec3(a.x, a.y, a.depth);
	glm::vec3 forward = glm::normalize(camera - vec_a);
	glm::vec3 right = glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), forward);
	right = glm::normalize(right);
	glm::vec3 up = glm::cross(forward, right);
	up = glm::normalize(up);
	orient = glm::mat3(right, up, forward);
}

void orbit_x(DrawingWindow &window) {
	for (int i = 0; i < (360/5); i++) {
		rotate_x(5);
		lookat(CanvasPoint(0, 0, 0));
		window.clearPixels();
		raster(window);
		window.renderFrame();
	}
}

void orbit_y(DrawingWindow &window) {
	for (int i = 0; i < (360/10); i++) {
		rotate_y(10);
		lookat(CanvasPoint(0, 0, 0));
		window.clearPixels();
		raytrace(window);
		window.renderFrame();
	}
}

void update(DrawingWindow &window) {
	// Function for performing animation (shifting artifacts or moving the camera)
}

void handleEvent(SDL_Event event, DrawingWindow &window) {
	if (event.type == SDL_KEYDOWN) {
		if (event.key.keysym.sym == SDLK_RIGHT) {
			camera.x = camera.x - 0.1;
			vector<CanvasTriangle> a = modelToCanvasPoints(triangles);
			window.clearPixels();
			init_buffer();
			for (int i = 0; i < a.size(); i++) {
				if (a[i].vertices[0].texturePoint.x != -1) textureMapping(window, a[i]);
				else filledTriangle(window, a[i], triangles[i].colour);
			}
		}
		else if (event.key.keysym.sym == SDLK_LEFT) {
			camera.x = camera.x + 0.1;
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_DOWN) {
			camera.y = camera.y + 0.1;
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_UP) {
			camera.y = camera.y - 0.1;
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_i) {
			camera.z = camera.z - 0.1;
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_k) {
			camera.z = camera.z + 0.1;
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_w) {
			rotate_x(-1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_s) {
			rotate_x(1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_a) {
			rotate_y(-1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_d) {
			rotate_y(1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_y) {
			orient_x(-1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_h) {
			orient_x(1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_g) {
			orient_y(-1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_j) {
			orient_y(1);
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_u) {
			CanvasPoint a = CanvasPoint(rand()%WIDTH, rand()%HEIGHT, -1.0);
			CanvasPoint b = CanvasPoint(rand()%WIDTH, rand()%HEIGHT, -1.0);
			CanvasPoint c = CanvasPoint(rand()%WIDTH, rand()%HEIGHT, -1.0);
			CanvasTriangle t = CanvasTriangle(a, b, c);
			Colour colour = Colour(rand()%256, rand()%256, rand()%256);
			strokedTriangle(window, t, colour);
		}
		else if (event.key.keysym.sym == SDLK_f) {
			CanvasPoint a = CanvasPoint(rand()%WIDTH, rand()%HEIGHT, -1.0);
			CanvasPoint b = CanvasPoint(rand()%WIDTH, rand()%HEIGHT, -1.0);
			CanvasPoint c = CanvasPoint(rand()%WIDTH, rand()%HEIGHT, -1.0);
			CanvasTriangle t = CanvasTriangle(a, b, c);
			Colour colour = Colour(rand()%256, rand()%256, rand()%256);
			filledTriangle(window, t, colour);
		}
		else if (event.key.keysym.sym == SDLK_t) {
			CanvasPoint a = CanvasPoint(160, 10, -1.0);
			a.texturePoint = TexturePoint(195, 5);
			CanvasPoint b = CanvasPoint(300, 230, -1.0);
			b.texturePoint = TexturePoint(395, 380);
			CanvasPoint c = CanvasPoint(10, 150, -1.0);
			c.texturePoint = TexturePoint(65, 330);
			CanvasTriangle t = CanvasTriangle(a, b, c);
			texture = TextureMap("texture.ppm");
			textureMapping(window, t);
		}
		else if (event.key.keysym.sym == SDLK_o) {
			orbit_x(window);
		}
		else if (event.key.keysym.sym == SDLK_p) {
			orbit_y(window);
		}
		else if (event.key.keysym.sym == SDLK_l) {
			loadOBJ("cornell-box-with-objects.obj", 0.17f);
			//loadOBJ("new_sphere.obj", 0.07f);
			set_normal();
			set_vnormal();
			//light.x = -0.289832f;
			//light.y = 0.455616f;
			//light.z = 0.701195f;
			cout << "Loaded" << endl;
		}
		else if (event.key.keysym.sym == SDLK_q) {
			lookat(CanvasPoint(0, 0, 0));
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_v) {
			window.clearPixels();
			raster(window);
		}
		else if (event.key.keysym.sym == SDLK_b) {
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_n) {
			window.clearPixels();
			wireframe(window);
		}
		else if (event.key.keysym.sym == SDLK_1) {
			light.x += 0.1;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_2) {
			light.x -= 0.1;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_3) {
			light.y += 0.1;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_4) {
			light.y -= 0.1;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_5) {
			light.z += 0.1;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_6) {
			light.z -= 0.1;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_7) {
			light.y += 0.01;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_8) {
			light.y -= 0.01;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_9) {
			light.x += 0.01;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_0) {
			light.x -= 0.01;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_COMMA) {
			focal_length += 0.5;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_PERIOD) {
			focal_length -= 0.5;
			window.clearPixels();
			raytrace(window);
		}
		else if (event.key.keysym.sym == SDLK_c) {
			init_buffer();
			window.clearPixels();
		}
	} else if (event.type == SDL_MOUSEBUTTONDOWN) window.savePPM("output.ppm");
}

int main(int argc, char *argv[]) {
	srand(time(0));
	DrawingWindow window = DrawingWindow(WIDTH, HEIGHT, false);
	SDL_Event event;
	init_buffer();
	while (true) {
		// We MUST poll for events - otherwise the window will freeze !
		if (window.pollForInputEvents(event)) handleEvent(event, window);
		update(window);
		// Need to render the frame at the end, or nothing actually gets shown on the screen !
		window.renderFrame();
	}
}
