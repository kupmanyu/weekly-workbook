#include <CanvasTriangle.h>
#include <DrawingWindow.h>
#include <Utils.h>
#include <fstream>
#include <vector>
#include <glm/glm.hpp>
#include <CanvasPoint.h>
#include <Colour.h>

#define WIDTH 320
#define HEIGHT 240

using namespace std;

vector<float> interpolateSingleFloats(float from, float to, int numberOfValues) {
	float step = (to - from) / (numberOfValues - 1);
	vector<float> result;
	result.push_back(from);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(result[i-1] + step);
	}
	return result;
}

vector<glm::vec3> interpolateThreeElementValues(glm::vec3 from, glm::vec3 to, int numberOfValues) {
	glm::vec3 step = glm::vec3(
		(to.x - from.x) / (numberOfValues - 1),
		(to.y - from.y) / (numberOfValues - 1),
		(to.z - from.z) / (numberOfValues - 1)
	);
	vector<glm::vec3> result;
	result.push_back(from);
	for (int i = 1; i < numberOfValues; i++) {
		result.push_back(glm::vec3(result[i-1].x + step.x, result[i-1].y + step.y, result[i-1].z + step.z));
	}
	return result;
}

uint32_t pack(float alpha, float red, float green, float blue) {
	return (int(alpha) << 24) + (int(red) << 16) + (int(green) << 8) + int(blue);
}

uint32_t pack_vec3(float alpha, glm::vec3 rgb) {
	return (int(alpha) << 24) + (int(rgb.r) << 16) + (int(rgb.g) << 8) + int(rgb.b);
}

vector<uint32_t> pack_vector(vector<glm::vec3> rgbs) {
	vector<uint32_t> result;
	for (int i = 0; i < rgbs.size(); i++) {
		result.push_back(pack_vec3(255.0, rgbs[i]));
	}
	return result;
}

vector<vector<uint32_t>> greyscale() {
	vector<vector<uint32_t>> colours;
	vector<float> w2b = interpolateSingleFloats(255, 0, WIDTH);
	vector<uint32_t> temp;
	for (int i = 0; i < w2b.size(); i++) {
		temp.push_back(pack(255, w2b[i], w2b[i], w2b[i]));
	}
	for (int i = 0; i < HEIGHT; i++) {
		colours.push_back(temp);
	}
	return colours;
}

vector<vector<uint32_t>> colourscale() {
	vector<vector<uint32_t>> colours;
	glm::vec3 topLeft(255, 0, 0);        // red
	glm::vec3 topRight(0, 0, 255);       // blue
	glm::vec3 bottomRight(0, 255, 0);    // green
	glm::vec3 bottomLeft(255, 255, 0);   // yellow
	vector<glm::vec3> top2botL = interpolateThreeElementValues(topLeft, bottomLeft, HEIGHT);
	vector<glm::vec3> top2botR = interpolateThreeElementValues(topRight, bottomRight, HEIGHT);
	vector<glm::vec3> temp;
	for (int i = 0; i < HEIGHT; i++) {
		temp = interpolateThreeElementValues(top2botL[i], top2botR[i], WIDTH);
		colours.push_back(pack_vector(temp));
	}
	return colours;
}

void draw(DrawingWindow &window, vector<vector<uint32_t>> colours) {
	window.clearPixels();
	for (size_t y = 0; y < window.height; y++) {
		for (size_t x = 0; x < window.width; x++) {
			window.setPixelColour(x, y, colours[y][x]);
		}
	}
}

void update(DrawingWindow &window) {
	// Function for performing animation (shifting artifacts or moving the camera)
}

void handleEvent(SDL_Event event, DrawingWindow &window) {
	if (event.type == SDL_KEYDOWN) {
		if (event.key.keysym.sym == SDLK_LEFT) std::cout << "LEFT" << std::endl;
		else if (event.key.keysym.sym == SDLK_RIGHT) std::cout << "RIGHT" << std::endl;
		else if (event.key.keysym.sym == SDLK_UP) std::cout << "UP" << std::endl;
		else if (event.key.keysym.sym == SDLK_DOWN) std::cout << "DOWN" << std::endl;
	} else if (event.type == SDL_MOUSEBUTTONDOWN) window.savePPM("output.ppm");
}

int main(int argc, char *argv[]) {
	vector<vector<uint32_t>> colours = colourscale();
	DrawingWindow window = DrawingWindow(WIDTH, HEIGHT, false);
	SDL_Event event;
	while (true) {
		// We MUST poll for events - otherwise the window will freeze !
		if (window.pollForInputEvents(event)) handleEvent(event, window);
		update(window);
		draw(window, colours);
		// Need to render the frame at the end, or nothing actually gets shown on the screen !
		window.renderFrame();
	}
}
